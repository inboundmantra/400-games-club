Games Club Landing Page
===================


This is a template for a landing page free to use for our partners.

----------


Actions to take
-------------

1. Update to your graphical profile
2. Replace the people in the comments section with localized ones.
3. Replace the games and the currency with local relevant ones.
4. Implement the "Enter phone number".
5. Decide if you want to run A/B testing


A/B testing
-------------

A proposal for A/B testing is to run a very simple version of the longer page to compare which one converts the test. The simple version of the page can be found in /simple/index.html. You have 2 alternatives:

1. Use the simple version found in /simple/index.html and adapt it to your style.
2. Use the longer version and use a query parameter to hide the rest of the content.


### Support

tobias.ekblom@appland.se
