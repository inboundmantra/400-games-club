jQuery(document).ready(function ($) {
	$('.games-club-landing-page-widget.game-slider').slick({
		arrows:true,
		centerMode: true,
		centerPadding: '25%',
		slidesToShow: 1,
		accessibility: false,
		autoplay: false,
		dots: true,
		dotsClass: 'games-club-landing-page-widget slick-dots',
		responsive: [
			{
				breakpoint: 965,
				settings: {
					arrows: false,
					infinite: false,
					dots: false,
					centerPadding: '4vw',
					slidesToShow: 1,
					slidesToScroll: 1
				}
			}
		]
	});
	$('.games-club-landing-page-widget.comments').slick({
		infinite: true,
		slidesToShow: 3,
		slidesToScroll: 3,
		arrows: false,
		responsive: [
			{
				breakpoint: 960,
				settings: {
					arrows: false,
					dots: true,
					dotsClass: 'games-club-landing-page-widget slick-dots',
					slidesToShow: 1,
					slidesToScroll: 1,
					autoplay:true
				}
			}
		]
	});

});
